# Coding Scheme Module 2: App Development

### Introduction

This is an expenses logging app, which allows a user to:

1. Take photos of their receipts
2. Save them with a timestamp of when they were taken
3. Labelling how much money is to be claimed from each receipt (manually entered by user)
4. Display a running total of the amount to be claimed

This app has been built using Angular and Ionic framework, developed on Windows 10 and tested in Android Studio using a Pixel 2 virtual device.

The app has also been compiled and loaded on a physical Android device for testing but not debugging due to issues with the devices USB data connection to ADB.

### Known Issues

- Loading an expense directly eg, `/expenses/<expense id>` causes the web app to hang when using a browser. I have been unable to correct this issue.
- When running on a OnePlus 5 A5000 device the pictures do not display on the details page. The app works as expected on the emulator and in the browser as a PWA.
