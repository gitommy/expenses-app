import { Injectable } from "@angular/core";

import { Plugins } from "@capacitor/core";

import { Expense } from "../models/expense.model";
import { Subject } from "rxjs";

import { PhotoService } from "./photo.service";

const { Storage } = Plugins;

@Injectable({
  providedIn: "root",
})
export class ExpensesService {
  constructor(private photoService: PhotoService) {
    this.readExpensesFromDisk();
  }

  public expensesChanged = new Subject<Expense[]>();
  private EXPENSES_STORAGE: string = "expenses";
  private _expenses: Expense[] = [];
  private _expenses_: Expense[] = [
    new Expense(
      "e1",
      "Sainsburys",
      "On the go lunch",
      3,
      new Date(2020, 3, 27, 14, 13).getTime(),
      false,
      "https://media.istockphoto.com/photos/shopping-receipt-picture-id901964616?k=6&m=901964616&s=612x612&w=0&h=RmFpYy9uDazil1H9aXkkrAOlCb0lQ-bHaFpdpl76o9A=",
      ""
    ),
    new Expense(
      "e2",
      "Tesco",
      "Cheeky dinner",
      6.99,
      new Date(2020, 2, 27, 11, 30).getTime(),
      true,
      "https://media.istockphoto.com/photos/shopping-receipt-picture-id901964616?k=6&m=901964616&s=612x612&w=0&h=RmFpYy9uDazil1H9aXkkrAOlCb0lQ-bHaFpdpl76o9A=",
      ""
    ),
    new Expense(
      "e3",
      "The Communications Inn",
      "Leaving meal for Dave",
      15.67,
      new Date(2020, 1, 15, 9, 0).getTime(),
      false,
      "https://media.istockphoto.com/photos/shopping-receipt-picture-id901964616?k=6&m=901964616&s=612x612&w=0&h=RmFpYy9uDazil1H9aXkkrAOlCb0lQ-bHaFpdpl76o9A=",
      ""
    ),
    new Expense(
      "e4",
      "Nandos",
      "Cheeky Nandos :-)",
      66.6,
      new Date(2019, 1, 15, 9, 0).getTime(),
      false,
      "1588680732992.jpeg",
      ""
    ),
  ];

  /**
   * Getter to return all expenses
   * @returns {Array} Array of expenses
   */
  get expenses() {
    return [...this._expenses];
  }

  /**
   *
   * @param {String} id The ID of the expense to return
   * @returns {Object} Object containing an expense
   */
  getExpense(id: string) {
    return { ...this._expenses.find((e) => e.id === id) };
  }

  /**
   * Generate a new random ID in Hex format based off Math.random()
   * @returns {String} The generated ID
   */
  generateRandomId() {
    // Generate a random Base16 (Hex) number that starts with an 'e'
    return Math.random().toString(16).replace("0.", "e");
  }

  /**
   * Add a new expense
   * @param {Expense} expense The new expense to be added
   */
  newExpense(expense: Expense) {
    this._expenses.push(expense);
    this.expensesChanged.next(this._expenses);
    // console.log(this._expenses);
    this.PersistExpensesToDisk();
  }

  /**
   * Updates the claimed status
   * @param {String} id ID of the expense
   * @param {Boolean} value True for claimed, False for unclaimed
   */
  updateClaimed(id: string, value: boolean) {
    this._expenses.forEach((el) => {
      if (el.id == id) {
        el.claimed = value;
      }
    });
    this.PersistExpensesToDisk();
  }

  /**
   * Saves the expenses array to disk
   */
  private PersistExpensesToDisk() {
    Storage.set({
      key: this.EXPENSES_STORAGE,
      value: JSON.stringify(this.expenses),
    });
  }

  /**
   * Reads the stored expenses array from disk
   */
  private async readExpensesFromDisk() {
    // console.log("*** LOADING EXPENSES ***");
    const readExpenses = await Storage.get({ key: this.EXPENSES_STORAGE });
    this._expenses = JSON.parse(readExpenses.value) || [];
    this.expensesChanged.next(this._expenses);
    // console.log(this._expenses);
    // console.log("*** EXPENSES LOADED ***");
  }

  /**
   * Deletes an expense
   * @param {String} id ID of the expense to delete
   */
  deleteExpense(id: string) {
    const expenseToDelete = this.getExpense(id);
    // Delete the photo
    this.photoService.deletePhoto(expenseToDelete.photoLocation);
    // Delete the expense
    this._expenses = this._expenses.filter((el) => {
      return el.id !== id;
    });
    // Persist expenses to disk
    this.PersistExpensesToDisk();
    // Push the changes out to subscribers
    this.expensesChanged.next(this._expenses);
  }
}
