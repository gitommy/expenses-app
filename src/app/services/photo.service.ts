import { Injectable } from "@angular/core";
import { Platform } from "@ionic/angular";

import {
  Plugins,
  CameraResultType,
  Capacitor,
  FilesystemDirectory,
  CameraPhoto,
  CameraSource,
} from "@capacitor/core";

import { Photo } from "../models/photo.model";

const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: "root",
})
export class PhotoService {
  constructor(platform: Platform) {
    this.platform = platform;
    this.loadSaved();
  }

  private photos: Photo[] = [];
  private PHOTO_STORAGE: string = "photos";

  public capturedPhoto: Photo[] = []; // Store a the latest captured photo
  private lastCameraPhoto: CameraPhoto;
  private platform: Platform;

  /** Uses the camera to take a photo
   * @returns {Photo[]} An object containing filepath, base64 and webPathView
   */
  public async addNewToGallery() {
    // Take a photo
    this.lastCameraPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100,
    });
    // Temp. store the photo, clear the capturedPhoto so we only store a
    // single phoet before adding a new one
    this.capturedPhoto = [];
    this.capturedPhoto.unshift({
      filepath: "soon...",
      webviewPath: this.lastCameraPhoto.webPath,
    });
    return this.capturedPhoto[0];
    // this.photos.unshift({
    //   filepath: "soon...",
    //   webviewPath: capturedPhoto.webPath,
    // });
  }

  // public async savePictureNew() {
  //   // This function was used to test a possible fix for the pictures not
  //   // displaying on my test device(worked on emulator) but still had the
  //   // same issue of the picture not displaying on the detail view when using OnePlus A5000
  //   console.log(this.lastCameraPhoto.path);
  //   const photoInTempStorage = await Filesystem.readFile({
  //     path: this.lastCameraPhoto.path,
  //   });
  //   const fileName = new Date().getTime() + ".jpeg";
  //   await Filesystem.writeFile({
  //     path: fileName,
  //     data: photoInTempStorage.data,
  //     directory: FilesystemDirectory.Data,
  //   });
  //   const finalPhotoUri = await Filesystem.getUri({
  //     directory: FilesystemDirectory.Data,
  //     path: fileName,
  //   });
  //   if (this.platform.is("hybrid")) {
  //     // Display the new image by rewriting the 'file://' path to HTTP
  //     // Details: https://ionicframework.com/docs/building/webview#file-protocol
  //     this.photos.unshift({
  //       filepath: finalPhotoUri.uri,
  //       webviewPath: Capacitor.convertFileSrc(finalPhotoUri.uri),
  //     });
  //   } else {
  //     // Use webPath to display the new image instead of base64 since it's
  //     // already loaded into memory
  //     this.photos.unshift({
  //       filepath: fileName,
  //       webviewPath: this.lastCameraPhoto.webPath,
  //     });
  //   }
  //   this.persistPhotosArrayToDisk();
  //   console.log(this.photos);
  //   return this.photos[0].filepath;
  // }

  /**
   * Saves a last photo taken to disk
   * @returns {String} The filepath of the stored photo
   */
  public async savePicture() {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(this.lastCameraPhoto);

    // Write the file to the data directory
    try {
      const fileName = new Date().getTime() + ".jpeg";
      const savedFile = await Filesystem.writeFile({
        path: fileName,
        data: base64Data,
        directory: FilesystemDirectory.Data,
      });
      if (this.platform.is("hybrid")) {
        // Display the new image by rewriting the 'file://' path to HTTP
        // Details: https://ionicframework.com/docs/building/webview#file-protocol
        this.photos.unshift({
          filepath: savedFile.uri,
          webviewPath: Capacitor.convertFileSrc(savedFile.uri),
        });
      } else {
        // Use webPath to display the new image instead of base64 since it's
        // already loaded into memory
        this.photos.unshift({
          filepath: fileName,
          webviewPath: this.lastCameraPhoto.webPath,
        });
      }
    } catch (err) {
      console.log("savePicture: writeFile error: " + err);
    }
    this.persistPhotosArrayToDisk();
    // console.log(this.photos);
    return this.photos[0].filepath;
  }

  /**
   * Saves the photos array to disk
   */
  private persistPhotosArrayToDisk() {
    Storage.set({
      key: this.PHOTO_STORAGE,
      value: this.platform.is("hybrid")
        ? JSON.stringify(this.photos)
        : JSON.stringify(
            this.photos.map((p) => {
              // Don't save the base64 representation of the photo data,
              // since it's already saved on the Filesystem
              const photoCopy = { ...p };
              delete photoCopy.base64;

              return photoCopy;
            })
          ),
    });
  }

  /**
   * Reads a file from disk into Base64
   * @param cameraPhoto Iinput photo of type CameraPhoto
   * @returns {String} Base64 string of the read file
   */
  private async readAsBase64(cameraPhoto: CameraPhoto) {
    // "hybrid" will detect Cordova or Capacitor
    if (this.platform.is("hybrid")) {
      // Read the file into base64 format
      try {
        const file = await Filesystem.readFile({
          path: cameraPhoto.path,
        });
        return file.data;
      } catch (err) {
        console.log("readAsBase64: readFile error: " + err);
      }
    } else {
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(cameraPhoto.webPath);
      const blob = await response.blob();

      return (await this.convertBlobToBase64(blob)) as string;
    }
  }

  /**
   * Converts a blob into Base64 format
   */
  convertBlobToBase64 = (blob: Blob) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.readAsDataURL(blob);
    });

  /**
   * Loads the stored photos array from disk
   */
  public async loadSaved() {
    // console.log("*** LOADING PHOTOS ***");
    // Retrieve stored photo array data
    const photos = await Storage.get({ key: this.PHOTO_STORAGE });
    this.photos = JSON.parse(photos.value) || [];

    // Easiest way to detect when running on the web:
    // “when the platform is NOT hybrid, do this”
    if (!this.platform.is("hybrid")) {
      // Display the photo by reading into base64 format
      for (let photo of this.photos) {
        // Read each saved photo's data from the Filesystem
        try {
          const readFile = await Filesystem.readFile({
            path: photo.filepath,
            // directory: FilesystemDirectory.Data, // Needs to be commented out to stop a file not found error
          });
          // Web platform only: Save the photo into the base64 field
          photo.base64 = `data:image/jpeg;base64,${readFile.data}`;
        } catch (err) {
          console.log("readFile error: " + err);
        }
      }
    }
    // console.log(this.photos);
    // console.log("*** PHOTOS LOADED ***");
  }

  // public getPhotoFromArray(fileName: String) {
  //   return this.photos.find((el) => {
  //     if (el.filepath === fileName) {
  //       console.log(el);
  //     }
  //     return el.filepath === fileName;
  //   });
  // }

  /**
   *
   * @param fileName The file name of the photo to retrieve
   * @returns {Photo} Object containing filepath, base64 and webViewPath
   */
  public getPhotoFromArray(fileName: String) {
    return { ...this.photos.find((el) => el.filepath === fileName) };
  }

  /**
   * Deletes a photo from disk and updates the photo object
   * @param {String} fileName The name of the file to delete
   */
  public async deletePhoto(fileName: string) {
    // First, delete the image from disk
    const deletedFile = await Filesystem.deleteFile({
      path: fileName,
      directory: FilesystemDirectory.Data,
    });
    // Second, delete from the photos array
    this.photos = this.photos.filter((photo) => {
      return photo.filepath !== fileName;
    });
    // Third, save the photos array to disk
    this.persistPhotosArrayToDisk();
  }
}
