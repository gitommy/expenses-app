import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastController, AlertController } from "@ionic/angular";

import { ExpensesService } from "../../services/expenses.service";
import { PhotoService } from "../../services/photo.service";
import { Expense } from "../../models/expense.model";
import { Photo } from "../../models/photo.model";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.page.html",
  styleUrls: ["./detail.page.scss"],
})
export class DetailPage implements OnInit {
  loadedExpense: Expense;
  expenseId: string;
  expenseClaimed: boolean;
  photo: Photo;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private expensesService: ExpensesService,
    private photoService: PhotoService,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("expenseId")) {
        console.log(
          "expenseId not found in paramMap. Redirecting to /expenses"
        );
        this.router.navigateByUrl("/expenses");
        return;
      }
      const expenseId = paramMap.get("expenseId");
      this.loadedExpense = this.expensesService.getExpense(expenseId);
      this.expenseId = this.loadedExpense.id;
      this.expenseClaimed = this.loadedExpense.claimed;
      /**
       * TODO:
       * Fix the error on this page whereby the details are not loaded correctly
       * if the user navigates directly to this page. The bug is caused by Angular
       * not re-rendering the page once the data has been retrived.
       * The bug only effects the web app as you cannot reach this page directly in
       * a native mobile add due to not having an address bar.
       */
      // this.photoService.loadSaved(); // Load here as well in case we land directly on this page
      // console.log("Getting photo: " + this.loadedExpense.photoLocation);
      this.photo = this.photoService.getPhotoFromArray(
        this.loadedExpense.photoLocation
      );
      // console.log("Loaded items:");
      // console.log(this.loadedExpense);
      // console.log(this.photo);
    });
  }

  /**
   * Update the claimed status of the loaded expense
   */
  onClaimed() {
    try {
      this.expensesService.updateClaimed(this.expenseId, this.expenseClaimed);
      this.presentToast("Claimed status saved!", 2000);
    } catch (err) {
      console.log("onClaimed error: " + err);
      this.presentToast(
        "An error has occured, the details can be viewed in the colsole/debugger",
        3000
      );
    }
  }

  /**
   * Present an alert to the user to confirm delete of the expense
   */
  onDelete() {
    this.presentAlertConfirm();
  }

  /**
   * Delete the loaded expense
   */
  deleteExpense() {
    try {
      this.expensesService.deleteExpense(this.expenseId);
      this.router.navigateByUrl("/expenses");
    } catch (err) {
      console.log("onClaimed error: " + err);
      this.presentToast(
        "An error has occured, the details can be viewed in the colsole/debugger",
        3000
      );
    }
  }

  /**
   * Display a toast notification to the user
   * @param msg Message to display to the user
   * @param durationMs Duration in milliseconds for the toast notification
   */
  async presentToast(msg: string, durationMs: number) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: durationMs,
      position: "bottom",
    });
    toast.present();
  }

  /**
   * Present an alert to the user asking to confirm expense deletion
   */
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: "Confirm!",
      message: "Are you sure you want to delete this expense?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: "Okay",
          cssClass: "danger",
          handler: () => {
            this.deleteExpense();
          },
        },
      ],
    });

    await alert.present();
  }
}
