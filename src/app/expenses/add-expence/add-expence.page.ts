import { Component, OnInit } from "@angular/core";

import { AlertController } from "@ionic/angular";

import { ExpensesService } from "../../services/expenses.service";
import { PhotoService } from "../../services/photo.service";
import { Expense } from "../../models/expense.model";
import { Photo } from "../../models/photo.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-expence",
  templateUrl: "./add-expence.page.html",
  styleUrls: ["./add-expence.page.scss"],
})
export class AddExpencePage implements OnInit {
  formValid = false;
  photo: Photo;
  now = new Date().toISOString();
  expenseName: string = "";
  expenseDescription: string = "";
  expenseAmount: number = null;
  expenseDate: string = this.now;
  expenseTime: string = this.now;
  expenseClaimed: boolean = false;
  expensePhotoLocation: string = "";
  expenseLocation: string = "";

  constructor(
    private expensesService: ExpensesService,
    private photoService: PhotoService,
    private alertController: AlertController,
    private router: Router
  ) {}

  ngOnInit() {}

  /**
   * Resets all the fields back to the default values
   */
  resetFields() {
    const now = new Date().toISOString();
    this.expenseName = "";
    this.expenseDescription = "";
    this.expenseAmount = null;
    this.expenseDate = now;
    this.expenseTime = now;
    this.expenseClaimed = false;
    this.expensePhotoLocation = "";
    this.expenseLocation = "";
    this.photoService.capturedPhoto = [];
  }

  /**
   * Validates the entered information and displays an alert if validation fails
   */
  validateForm() {
    const alertHeader = "Validation failed";
    // Validate that name is present
    if (this.expenseName == "") {
      this.presentAlert(alertHeader, "Name is required!");
      return;
    }
    // validate amaount is > 0
    if (this.expenseAmount <= 0) {
      this.presentAlert(alertHeader, "Amount must be greater than 0 (zero)");
      return;
    }
    // Validate that the date is not in the future
    const now = Date.now();
    if (new Date(this.expenseDate).getTime() >= now) {
      this.presentAlert(alertHeader, "The date cannot be in the future");
      return;
    }
    if (new Date(this.expenseTime).getTime() >= now) {
      this.presentAlert(alertHeader, "The time cannot be in the future");
      return;
    }
    // Validate that we have a picture
    if (this.photoService.capturedPhoto.length <= 0) {
      this.presentAlert(alertHeader, "There is no photograph of the reciept");
      return;
    }
    // console.log("Form validation passed");
    this.formValid = true;
  }

  /**
   * Creates a new Expense and usses the expenses service to save and persist
   * the information. Once saved the fields are cleared and the user is navigated
   * back to the main /expenses screen
   */
  async onSave() {
    this.validateForm();
    if (this.formValid) {
      let expense: Expense;
      // console.log("Save button clicked");
      // console.log("Name: " + this.expenseName);
      // console.log("Desc: " + this.expenseDescription);
      // console.log("Amount: " + this.expenseAmount);
      // console.log("Date: " + this.expenseDate);
      // console.log("Time: " + this.expenseTime);
      const year = new Date(this.expenseDate).getFullYear();
      const month = new Date(this.expenseDate).getMonth();
      const date = new Date(this.expenseDate).getDate();
      const hours = new Date(this.expenseTime).getHours();
      const minutes = new Date(this.expenseTime).getMinutes();
      // console.log("year: " + year);
      // console.log("month: " + month);
      // console.log("day: " + day);
      // console.log("hour: " + hours);
      // console.log("minutes: " + minutes);
      this.expensePhotoLocation = await this.photoService.savePicture();
      expense = new Expense(
        this.expensesService.generateRandomId(),
        this.expenseName,
        this.expenseDescription,
        this.expenseAmount,
        new Date(year, month, date, hours, minutes).getTime(),
        this.expenseClaimed,
        this.expensePhotoLocation,
        this.expenseLocation
      );
      this.expensesService.newExpense(expense);
      // console.log(expense);
    }
    if (this.formValid == true) {
      this.resetFields();
      this.router.navigateByUrl("/expenses");
    }
  }

  /**
   * Uses the photo service to take a photo. The returned image is saved to
   * a local variable to be displayed on the page
   */
  onTakePhoto() {
    this.photoService.addNewToGallery().then((image) => {
      this.photo = image;
    });
  }

  /**
   * Tries to get and return the webviewPath for the photo, if unable returns
   * the path to a placeholder image
   * @returns {String} webpathView | path to placeholder image
   */
  getPhoto() {
    try {
      return this.photo.webviewPath;
    } catch {
      return "assets/image_placeholder.png";
    }
  }

  /**
   * Present an alert to the user
   * @param {String} header Title/header of the alert
   * @param {String} message Message to display in the alert box
   * @param {String} subHeader Optional: sub-header/title of the alert
   */
  async presentAlert(header: string, message: string, subHeader?: string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: ["OK"],
    });

    await alert.present();
  }
}
