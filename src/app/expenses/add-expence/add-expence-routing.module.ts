import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddExpencePage } from './add-expence.page';

const routes: Routes = [
  {
    path: '',
    component: AddExpencePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddExpencePageRoutingModule {}
