import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddExpencePageRoutingModule } from './add-expence-routing.module';

import { AddExpencePage } from './add-expence.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddExpencePageRoutingModule
  ],
  declarations: [AddExpencePage]
})
export class AddExpencePageModule {}
