import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddExpencePage } from './add-expence.page';

describe('AddExpencePage', () => {
  let component: AddExpencePage;
  let fixture: ComponentFixture<AddExpencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddExpencePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddExpencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
