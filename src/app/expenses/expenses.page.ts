import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

import { ExpensesService } from "../services/expenses.service";
import { Expense } from "../models/expense.model";
import { AlertController, ToastController } from "@ionic/angular";

@Component({
  selector: "app-expenses",
  templateUrl: "./expenses.page.html",
  styleUrls: ["./expenses.page.scss"],
})
export class ExpensesPage implements OnInit, OnDestroy {
  private expensesSub: Subscription;
  isFetching: boolean = false;
  expensesList: Expense[] = [];
  totalToClaim: number;
  totalClaimed: number;

  constructor(
    private expensesService: ExpensesService,
    private alertController: AlertController,
    private toastCtrl: ToastController
  ) {}

  ngOnInit() {
    this.expensesSub = this.expensesService.expensesChanged.subscribe(
      (expenses) => {
        this.expensesList = expenses;
        this.isFetching = false;
      }
    );
    this.isFetching = true;
    this.expensesList = this.expensesService.expenses;
    // console.log(this.expensesList);
  }

  ionViewWillEnter() {
    this.calculateTotals();
  }

  /**
   * Calculates the totals for both claimed and unclaimed expenses
   */
  calculateTotals() {
    this.totalToClaim = 0;
    this.totalClaimed = 0;
    this.expensesList.forEach((el) => {
      // console.log(el.amount);
      if (!el.claimed) {
        this.totalToClaim = this.totalToClaim + el.amount;
      } else {
        this.totalClaimed = this.totalClaimed + el.amount;
      }
    });
  }

  /**
   * Sorts the expensesList into a decending order
   * @param {String} prop Property to sort by eg, timestamp
   * @returns {Array} Sorted array
   */
  sortBy(prop: string) {
    return [...this.expensesList].sort((a, b) => b[prop] - a[prop]);
  }

  onClaimed(id: string, value: boolean) {
    this.expensesService.updateClaimed(id, value);
    this.calculateTotals();
  }

  /**
   * Presents an alert to the user asking to confirm deletion
   * @param {String} id Expense ID
   */
  onDelete(id: string) {
    // console.log("onDelete");
    this.presentAlertConfirm(id);
  }

  /**
   * Deletes an expense
   * @param {String} id Expense ID
   */
  deleteExpense(id: string) {
    this.expensesService.deleteExpense(id);
    this.presentToast("Expence sucessfully deleted!", 1000);
    this.calculateTotals();
  }

  /**
   * Presents an alert to the user asking the user to confirm if the expense
   * should be deleted
   * @param {Stirng} id Expense ID
   */
  async presentAlertConfirm(id: string) {
    const alert = await this.alertController.create({
      header: "Confirm!",
      message: "Are you sure you want to delete this expense?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: "Okay",
          cssClass: "danger",
          handler: () => {
            this.deleteExpense(id);
          },
        },
      ],
    });
    await alert.present();
  }

  /**
   * Present a toast popup to the user
   * @param {string} msg Message to display to the user
   * @param {Number} durationMs Duration in miliseconds the notification should
   *                            remain on screen
   */
  async presentToast(msg: string, durationMs: number) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: durationMs,
      position: "bottom",
    });
    toast.present();
  }

  ngOnDestroy() {
    this.expensesSub.unsubscribe();
  }
}
