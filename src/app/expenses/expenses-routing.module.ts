import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ExpensesPage } from "./expenses.page";

const routes: Routes = [
  {
    path: "",
    component: ExpensesPage,
  },
  {
    path: "add",
    loadChildren: () =>
      import("./add-expence/add-expence.module").then(
        (m) => m.AddExpencePageModule
      ),
  },
  {
    path: ":expenseId",
    loadChildren: () =>
      import("./detail/detail.module").then((m) => m.DetailPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpensesPageRoutingModule {}
