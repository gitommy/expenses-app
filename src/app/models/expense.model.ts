export class Expense {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public amount: number,
    public timestamp: number,
    public claimed: boolean,
    public photoLocation: string,
    public location: string
  ) {}
}
